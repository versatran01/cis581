function im = pyr_recon(pyr)
% adapted from Yan Ke @ THUEE, xjed09@gmail.com
level = length(pyr);
for l = level-1:-1:1
    pyr{l} = pyr{l} + pyr_exp(pyr{l+1});
end
im = pyr{1};

end