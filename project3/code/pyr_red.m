function im_out = pyr_red(im)
% Get next level of gaussian pyramid

% Create gaussian kernel
a = 0.375;
w = [1/4-a/2, 1/4, a, 1/4, 1/4-a/2];
g = conv2(w,w');

% Blur the image and subsample
% im_filtered = imfilter(im, g, 'replicate', 'same');
% im_out = im_filtered(1:2:end, 1:2:end, :);

im_out = impyramid(im, 'reduce');
end