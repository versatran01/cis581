function pyr = pyr_gen(img, level, type)

pyr = cell(1, level);
pyr{1} = img;
% Generate gaussian pyramid
for l = 2:level
    pyr{l} = pyr_red(pyr{l-1});
end
if strcmp(type, 'gauss'), return; end

% Generate lapacian pyramid
% Resize image to be 2*[nr nc] - 1
for l = level-1:-1:1
    sz = size(pyr{l+1})*2-1;
    pyr{l} = pyr{l}(1:sz(1), 1:sz(2), :);
end

% Expand image and then get difference
for l = 1:level-1
    pyr{l} = pyr{l} - pyr_exp(pyr{l+1});
end
end