function img_mosaic = mymosaic(img_input, method)
% Mosaic wrapper
if nargin < 2, method = 0; end
im = img_input;
max_pts = 200;
num_image = length(img_input);
mid_ind = ceil(num_image/2);
fprintf('%d input images\n', num_image);

% Convert rgb image to grayscale
for i = 1:num_image
    if size(im{i}, 3) == 3
        im{i} = rgb2gray(im{i});
    end
end

% precompute corner points and feature descriptors
for i = 1:num_image
    fprintf('Computing feature descriptor for the %dth image\n', i)
    cimg{i} = cornermetric(im{i}, 'Harris');
    [y{i}, x{i}, ~] = anms(cimg{i}, max_pts);
    p{i} = feat_desc(im{i}, y{i}, x{i});
end

% precomput homography matrix for
for i = 1:num_image - 1
    i1 = i;
    i2 = i + 1;
    fprintf('Computing homography matrix from image %d to %d \n', i2, i1)
    m = feat_match(p{i1}, p{i2});
    x1 = x{i1}(m > 0);
    y1 = y{i1}(m > 0);
    x2 = x{i2}(m(m > 0));
    y2 = y{i2}(m(m > 0));
    H{i} = ransac_est_homography(y1, x1, y2, x2, 2);
end

fprintf('Creating canvas with image %d.\n', mid_ind)
offset = [0 0];
img_mosaic = img_input{mid_ind};
im_mask = ones(size(img_mosaic,1), size(img_mosaic,2)) > 0;

fprintf('Starting image stitching from middle to right.\n')
Hacc = eye(3);
for i = mid_ind : num_image - 1
    fprintf('Stitching image %d on to canvas. ', i+1);tic;
    im_new = img_input{i + 1};
    Hacc = Hacc*H{i};
    [img_mosaic, im_mask, offset] = ...
    stitch(Hacc, img_mosaic, im_new, im_mask, offset, method);toc;
end

fprintf('Starting image stitching from middle to left.\n')
Hacc = eye(3);
for i = mid_ind -1:-1:1
    fprintf('Stitching image %d on to canvas. ', i);tic;
    im_new = img_input{i};
    Hacc = inv(H{i})*Hacc;
    [img_mosaic, im_mask, offset] = ...
    stitch(Hacc, img_mosaic, im_new, im_mask, offset, method);toc;
end

end
