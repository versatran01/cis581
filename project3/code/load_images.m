function Icell = load_images(path, visualize)
if nargin < 2, visualize = false; end
if nargin < 1, path = '../images/'; end

imHeight = 350;
imagePath = path;
imageExtension = {'.jpg', '.jpeg', '.png', '.tiff', '.bmp'};
fileListing = dir(imagePath);
numFile = length(fileListing);
Icell = cell(1, numFile);
Icount = 0;
for i = 1:numFile
    [~, ~, fileExtension] = fileparts(fileListing(i).name);
    % check if file is image
    if any(strcmp(fileExtension, imageExtension))
        Icount = Icount + 1;
        im = imread([imagePath fileListing(i).name]);
        if size(im, 2) > imHeight
            im = imresize(im, imHeight/size(im,2));
        end
        Icell{Icount} = im;
        
        if visualize
            figure();
            imshow(Icell{Icount})
            drawnow
            title(fileListing(i).name)
        end
        fprintf([fileListing(i).name, '\n'])
    end
end
Icell = Icell(1:Icount);
fprintf('Load %d images.\n', Icount);
close all
end
