function im_out = pyr_exp(im)

% Create gaussian kernel
a = 0.375;
w = [1/4-a/2, 1/4, a, 1/4, 1/4-a/2];
g = conv2(w,w');

% Use matlab's impyramid for test
im_out = impyramid(im, 'expand');
end