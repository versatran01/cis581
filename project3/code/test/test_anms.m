close all;
clear all;
clc;

n = 4;
%I = checkerboard(20, n);
Iall = load_images();
I = Iall{6};
% I = rgb2gray(I);
cimg = cornermetric(I, 'Harris');
cimg_adjusted = imadjust(cimg);
corners = detectHarrisFeatures(I);

%max_pts = (2*n - 1)^2;
corner_thresh = 0.01*max(cimg(:));
cimg(cimg < corner_thresh) = 0;
max_pts = 80;
bw = imregionalmax(cimg);
cimg_rm = bw.*cimg;
[r, c, v] = find(cimg_rm);
[v, idx] = sort(v);
r = r(idx);
c = c(idx);
d2 = bsxfun(@minus, r, r').^2 + bsxfun(@minus, c, c').^2;
d2_tril = tril(d2, -1);
d2_tril(d2_tril == 0) = Inf;
max_radii = min(d2_tril);
[max_radii, idx] = sort(max_radii, 'descend');
max_pts = min(max_pts, length(v));
y = r(idx(1:max_pts));
x = c(idx(1:max_pts));
rmax = sqrt(max_radii(max_pts));

%% Visualization
figure()
hold on
imshow(I);
plot(corners);
hold off
title('original image')

figure()
imshow(cimg_adjusted);
colormap(jet);
title('corner strength')

%figure()
%imshow(cimg > 0);
%title('corner before anms')

figure()
imshow(bw);
title('regional max')

figure()
imshow(cimg_adjusted);
hold on
plot(c, r, 'g+')
plot(x, y, 'ro')
hold off
title('regional max 2')

figure()
imshow(cimg_adjusted)
hold on
plot(x, y, 'r+')
hold off
title('corner after anms')

