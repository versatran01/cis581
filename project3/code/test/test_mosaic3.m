%% test mosiac 3
clear all
close all
clc

% load('images.mat')
Iall = load_images();
image_input = Iall(4:8);


% function
im = image_input;
max_pts = 200;
num_image = length(image_input);
fprintf('%d input images\n', num_image);

% Convert rgb image to grayscale
for i = 1:num_image
    if size(im{i}, 3) == 3
        im{i} = rgb2gray(im{i});
    end
end

% Get index of middle image
mid_ind = ceil(num_image/2);

% precompute corner points and feature descriptors
for i = 1:num_image
    fprintf('Computing feature descriptor for the %dth image\n', i)
    cimg{i} = cornermetric(im{i}, 'Harris');
    [y{i}, x{i}, ~] = anms(cimg{i}, max_pts);
    p{i} = feat_desc(im{i}, y{i}, x{i});
end

% precomput homography matrix for
for i = 1:num_image - 1
    i1 = i;
    i2 = i + 1;
    fprintf('Computing homography matrix from image %d to %d \n', i2, i1)
    m = feat_match(p{i1}, p{i2});
    x1 = x{i1}(m > 0);
    y1 = y{i1}(m > 0);
    x2 = x{i2}(m(m > 0));
    y2 = y{i2}(m(m > 0));
    H{i} = ransac_est_homography(y1, x1, y2, x2, 2);
end

offset = [0 0];
im_mosaic = image_input{mid_ind};
im_mask = ones(size(im_mosaic,1), size(im_mosaic,2));
im_new = image_input{mid_ind + 1};

[im_mosaic, im_mask, offset] = ...
    stitch2(H{mid_ind}, im_mosaic, im_new, im_mask, offset);

im_new = image_input{mid_ind + 2};
[im_mosaic, im_mask, offset] = ...
    stitch2(H{mid_ind}*H{mid_ind + 1}, im_mosaic, im_new, im_mask, offset);

im_new = image_input{mid_ind - 1};
[im_mosaic, im_mask, offset] = ...
    stitch2(inv(H{mid_ind - 1}), im_mosaic, im_new, im_mask, offset);

im_new = image_input{mid_ind - 2};
[im_mosaic, im_mask, offset] = ...
    stitch2(inv(H{mid_ind - 1}*H{mid_ind-2}), im_mosaic, im_new, im_mask, offset);

figure()
subplot(1,2,1)
imshow(im_mosaic)
subplot(1,2,2)
colormap(gray)
imagesc(im_mask)
axis image

%%



