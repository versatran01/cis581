clear all;
close all;
clc;

Iall = load_images();

%%
image_input = Iall(5:6);

%mymosaic
im = image_input;
max_pts = 200;
num_image = length(image_input);
[nr, nc, nt] = size(im{1});
% Convert rgb image to grayscale
if nt == 3
    for i = 1:num_image
        im{i} = rgb2gray(im{i});
    end
end
% Get index of middle image
mid_ind = ceil(num_image/2);
cimg = cell(1, num_image);
x = cell(1, num_image);
y = cell(1, num_image);
p = cell(1, num_image);
% Precompute corner points and feature descriptor
for i = 1:num_image
    cimg{i} = cornermetric(im{i}, 'Harris');
    [y{i}, x{i}, ~] = anms(cimg{i}, max_pts);
    p{i} = feat_desc(im{i}, y{i}, x{i});
end

old_H = eye(3);
old_im = image_input{mid_ind};
im1_row_offset = 0;
im1_col_offset = 0;
% Match from mid to right
for i = mid_ind : num_image - 1
    i1 = i;
    i2 = i + 1;
    m = feat_match(p{i1}, p{i2});
    x1 = x{i1}(m > 0);
    y1 = y{i1}(m > 0);
    x2 = x{i2}(m(m > 0));
    y2 = y{i2}(m(m > 0));
    thresh = 3;
    H = ransac_est_homography(y1, x1, y2, x2, thresh);
    H = old_H*H;
    old_H = H;
    % Start stitching
    [nr1, nc1, ~] = size(old_im);
    [nr2, nc2, ~] = size(im{i2});
    im2_corner_x = [1; 1; nc2; nc2];
    im2_corner_y = [1; nr2; nr2; 1];
    [im2_corner_X, im2_corner_Y] = ...
        apply_homography(H, im2_corner_x, im2_corner_y);
    im2_col_range = round([min([im2_corner_X; 1]), ...
                           max([im2_corner_X; nc1])]);
    im2_row_range = round([min([im2_corner_Y; 1]), ...
                           max([im2_corner_Y; nr1])]);
    [x2_d, y2_d] = meshgrid(im2_col_range(1):im2_col_range(2), ...
                            im2_row_range(1):im2_row_range(2));
    nr_out = im2_row_range(2) - im2_row_range(1) + 1;
    nc_out = im2_col_range(2) - im2_col_range(1) + 1;
    x2_d = x2_d(:);
    y2_d = y2_d(:);
    [x2_d_s, y2_d_s] = apply_homography(inv(H), x2_d, y2_d);
    im_out = zeros(nr_out, nc_out, 3, 'uint8');
    for j = 1:3
        tmp = interp2(double(image_input{i2}(:,:,j)), x2_d_s, y2_d_s, 'linear', 0);
        im_out(:,:,j) = reshape(tmp, nr_out, nc_out);
    end
    im1_row_offset = -im1_row_offset-min(im2_row_range(1), 0);
    im1_col_offset = -im1_col_offset-min(im2_col_range(1), 0);
    im_out((1:nr1)+im1_row_offset, (1:nc1)+im1_col_offset, :) ...
        = old_im;
    old_im = im_out;
end

%% Plotting
% num_sub = ceil(sqrt(num_image));
figure()
for i = 1:num_image
    subplot(1, num_image, i)
    imshow(image_input{i});
    axis image;
    title(sprintf('original image %d', i))
    set(gca, 'Visible', 'On')
end

figure()
imshow(im_out)



