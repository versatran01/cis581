close all;
clear all;
clc;

n = 4;
%max_pts = ceil((2*n - 1)^2/2);
max_pts = 25;
%im = checkerboard(20, n);

Iall = load_images();
im =  Iall{4};
im = rgb2gray(im);
cimg = cornermetric(im, 'Harris');
cimg_adjusted = imadjust(cimg);
corners = detectHarrisFeatures(im);

[y, x, rmax] = anms(cimg, max_pts);
% feat_desc
win_size = 40;
p = zeros(64, size(y,1));
g = fspecial('Gaussian', win_size, sqrt(2));
im_filtered = imfilter(im, g, 'symmetric', 'full');

half_win_size = ceil(win_size/2);
y1 = y + half_win_size;
x1 = x + half_win_size;
for i = 1:size(y,1)
    im_sub = im_filtered((y1(i) - half_win_size):(y1(i) + half_win_size - 1), (x1(i) - half_win_size):(x1(i) + half_win_size - 1));
    im_patch = im_sub(1:5:win_size, 1:5:win_size);
    p(:,i) = reshape(im_patch, 64, 1);
end

p = bsxfun(@minus, p, mean(p,1));
p = bsxfun(@rdivide, p, std(p, 1, 1));

%% Visualization
figure()
hold on
imshow(im);
plot(corners);
hold off
title('original image')

figure()
imshow(cimg_adjusted);
hold on
plot(x, y, 'g+')
hold off
title('corner strength')

figure()
a = ceil(sqrt(size(p,2)));
for i = 1:size(p,2)
    subplot(a, a, i)
    imshow(reshape(p(:,i), 8, 8));
end
