clear all;
close all;
clc;

Iall = load_images();
max_pts = 100;
Itest{1} = Iall{6};
Itest{2} = Iall{7};
for i = 1:2
    %Itest{i} = Iall{i};
    if size(Itest{i}, 3) == 3
        im{i} = rgb2gray(Itest{i});
    else
        im{i} = Itest{i};
    end
    cimg{i} = cornermetric(im{i}, 'Harris');
    cimg_adjusted{i} = imadjust(cimg{i});
    corners{i} = detectHarrisFeatures(im{i});
    [y{i}, x{i}, rmax{i}] = anms(cimg{i}, max_pts);
    p{i} = feat_desc(im{i}, y{i}, x{i});
end
m = feat_match(p{1}, p{2});
x1 = x{1}(m > 0);
y1 = y{1}(m > 0);
x2 = x{2}(m(m > 0));
y2 = y{2}(m(m > 0));
thresh = 5; % threshhold on distance
[H, inlier_ind] = ransac_est_homography(y1, x1, y2, x2, thresh);

[nr1, nc1] = size(im{1});
[nr2, nc2] = size(im{2});
im2_corner_x = [1; 1; nc2; nc2];
im2_corner_y = [1; nr2; nr2; 1];
[im2_corner_X, im2_corner_Y] = apply_homography(H, im2_corner_x, im2_corner_y);
im2_col_range = round([min([im2_corner_X; 1]), max([im2_corner_X; nc1])]);
im2_row_range = round([min([im2_corner_Y; 1]), max([im2_corner_Y; nr1])]);
[x2_d, y2_d] = meshgrid(im2_col_range(1):im2_col_range(2), ...
                        im2_row_range(1):im2_row_range(2));
nr_out = im2_row_range(2) - im2_row_range(1) + 1;
nc_out = im2_col_range(2) - im2_col_range(1) + 1;
x2_d = x2_d(:);
y2_d = y2_d(:);
[x2_d_s, y2_d_s] = apply_homography(inv(H), x2_d, y2_d);
im_out = zeros(nr_out, nc_out, 3, 'uint8');
for i = 1:3
    tmp = interp2(double(Itest{2}(:,:,i)), x2_d_s, y2_d_s, 'linear', 0);
    im_out(:,:,i) = reshape(tmp, nr_out, nc_out);
end
im1_row_offset = -min(im2_row_range(1), 0);
im1_col_offset = -min(im2_col_range(1), 0);
im_out((1:nr1)+im1_row_offset, (1:nc1)+im1_col_offset, :) = Itest{1};

figure()
imshow(im_out)

%% Ploting
%display test images
figure()
for i = 1:2
    subplot(1,2,i)
    imshow(Itest{i});
    hold on
    plot(corners{i})
    hold off
    axis image;
    title(sprintf('test image raw %d', i));
    set(gca, 'Visible', 'On')
end

%corner detection
figure()
for i = 1:2
    subplot(1,2,i)
    imshow(cimg_adjusted{i});
    hold on
    plot(x{i}, y{i}, 'g+')
    hold off
    axis image;
    title(sprintf('corner metric %d', i));
    set(gca, 'Visible', 'On')
end

%adaptive non-maximal suppression
figure()
for i = 1:2
    subplot(1,2,i)
    imshow(im{i});
    axis image;
    hold on
    plot(x{i}, y{i}, 'g+')
    hold off
    title(sprintf('corner after anms %d', i));
    set(gca, 'Visible', 'On')
end

% Feature match and ransac
[im_height, im_width] = size(im{1});
figure()
colormap(gray)
imagesc(0, 0, im{1})
axis image
hold on
imagesc(im_width, 0, im{2})
plot(x{1}, y{1}, 'g+')
plot(x{2}+im_width, y{2}, 'g+')
plot(x1, y1, 'ro')
plot(x2+im_width, y2, 'ro')
xline = [x1, x2 + im_width]';
yline = [y1, y2]';
line(xline, yline, 'Color', 'y')

x1_ransac = x1(inlier_ind);
x2_ransac = x2(inlier_ind);
y1_ransac = y1(inlier_ind);
y2_ransac = y2(inlier_ind);
xline_ransac = [x1_ransac, x2_ransac + im_width]';
yline_ransac = [y1_ransac, y2_ransac]';
line(xline_ransac, yline_ransac, 'Color', 'm')
hold off
axis([0 im_width*2 0 im_height])
