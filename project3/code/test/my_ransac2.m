function best_inlier = my_ransac2(p,p_dot,z,iter,epsilon,thresh)
% MY_RANSAC2 RANSAC algorithm with collinearity check
% best_inlier = my_ransac(p,p_dot,z,iter,e)
% iter    = 60;
% epsilon = 0.1^2;
% thresh  = 0.9;

% Initialization
num_data    = size(p,2);
ind         = 1:num_data;
best_inlier = [];
r           = sqrt(eps);
% RANSAC
for i = 1:iter
    is_colnr    = 1;
    while is_colnr
        % Generate three random indices
        perm        = randperm(num_data);
        rand_ind    = perm(1:3);
        % D = FX
        D_rand      = p_dot(:,rand_ind);
        D_rand      = D_rand(:);
        p_rand      = p(:,rand_ind);
        x_rand      = p_rand(1,:)';
        y_rand      = p_rand(2,:)';
        z_rand      = z(rand_ind)';
        p_rand_h    = [p_rand; 1 1 1];
        is_colnr    = ...
            (norm(cross(p_rand_h(:,2)-p_rand_h(:,3),...
            p_rand_h(:,1)-p_rand_h(:,2))) < r);
    end
    F_rand      = my_func_vel(x_rand,y_rand,z_rand);
    X_est       = F_rand\D_rand;

    x1          = p(1,:)';
    y1          = p(2,:)';
    z1          = z';
    F1          = my_func_vel(x1,y1,z1);
    D1          = F1*X_est;
    p_dot_est   = reshape(D1,2,num_data);
    p_dot_diff  = sum((p_dot_est - p_dot).^2,1);
    inlier      = ind(p_dot_diff < epsilon);
    k           = size(inlier,2);

    % Exit if almost every data point is inlier
    if  k > (num_data*thresh)
        best_inlier = inlier;
        break
    else
    % Otherwise keep iterating
        if k > size(best_inlier,2)
            best_inlier = inlier;
        end
    end
end
end
