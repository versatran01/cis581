%function [H, inilier_ind] = ransac_est_homography(y1, x1, y2, x2, thresh)
close all
clear all
clc

Iall = load_images();
max_pts = 100;
for i = 1:2
    Itest{i} = Iall{i+3};

    im{i} = rgb2gray(Itest{i});
    cimg{i} = cornermetric(im{i}, 'Harris');
    cimg_adjusted{i} = imadjust(cimg{i});
    corners{i} = detectHarrisFeatures(im{i});
    [y{i}, x{i}, rmax{i}] = anms(cimg{i}, max_pts);
    p{i} = feat_desc(im{i}, y{i}, x{i});
end

m = feat_match(p{1}, p{2});
x1 = x{1}(m > 0);
y1 = y{1}(m > 0);
x2 = x{2}(m(m > 0));
y2 = y{2}(m(m > 0));
thresh = 5; % threshhold on distance

percent_inlier = 0.98;
iter = 100;
inlier_ind = [];
num_data = size(y1, 1);
ind = 1:num_data;

% RANSAC
for i = 1:iter
    perm = randperm(num_data);
    rand_ind = perm(1:4);
    x1_rand = x1(rand_ind);
    y1_rand = y1(rand_ind);
    x2_rand = x2(rand_ind);
    y2_rand = y2(rand_ind);
    Hest = est_homography(x1_rand, y1_rand, x2_rand, y2_rand);
    [x1_est, y1_est] = apply_homography(Hest, x2, y2);
    dist = (x1_est - x1).^2 + (y1_est- y1).^2;
    inlier = ind(dist < thresh^2);
    num_inlier = length(inlier);

    if num_inlier > num_data * percent_inlier
        inlier_ind = inlier;
        H = est_homography(x1(inlier_ind), y1(inlier_ind), x2(inlier_ind), y2(inlier_ind));
        disp(['Finished at iteration ', num2str(iter)])
        break
    elseif num_inlier > length(inlier_ind)
        inlier_ind = inlier;
        H = est_homography(x1(inlier_ind), y1(inlier_ind), x2(inlier_ind), y2(inlier_ind));
    end
end
disp(['Finished at iteration', num2str(iter)])

%display test images
figure()
for i = 1:2
    subplot(1,2,i)
    imshow(Itest{i});
    hold on
    plot(corners{i})
    hold off
    axis image;
    title(sprintf('test image raw %d', i));
    set(gca, 'Visible', 'On')
end

figure()
colormap(gray)
[im_height, im_width] = size(im{1});
imagesc(0, 0, im{1})
axis image
hold on
imagesc(im_width, 0, im{2})
plot(x{1}, y{1}, 'g+')
plot(x{2}+im_width, y{2}, 'g+')
plot(x1, y1, 'ro')
plot(x2+im_width, y2, 'ro')
xline = [x1, x2 + im_width]';
yline = [y1, y2]';
line(xline, yline, 'Color', 'y')
x1_ransac = x1(inlier_ind);
x2_ransac = x2(inlier_ind);
y1_ransac = y1(inlier_ind);
y2_ransac = y2(inlier_ind);
xline_ransac = [x1_ransac, x2_ransac + im_width]';
yline_ransac = [y1_ransac, y2_ransac]';
line(xline_ransac, yline_ransac, 'Color', 'm')
hold off
axis([0 im_width*2 0 im_height])
