clear all
close all
clc
im = imread('../images/lenna.png');
imshow(im)
im = rgb2gray(im);

g = fspecial('gaussian', 5, 1);

im1 = imfilter(im, g, 'conv');
imshow(im1)
im1 = im1(1:2:end, 1:2:end, :);
imshow(im1)
im1 = imresize(im1, 2);
imshow(im1)
lap1 = im - im1;
imshow(lap1)
colormap(gray)
