function [I, Imask, offset] = stitch2b(H, I1, I2, I1mask, offset)
% Stitch I2 with I1, where I1 is in canvas and H12 transform I2 into
% canvas frame
% offset is [x, y] >= 0
% (xc, yc) = (xm, ym) + offset

% Get corner of I2
[nr1, nc1, ~] = size(I1);
[nr2, nc2, ~] = size(I2);
I1_corner = bbox([1, 1], [nc1, nr1]);
I2_corner = bbox([1, 1], [nc2, nr2]);

% Use homography matrix to transform I2 corner in to I1
[I2_corner_X, I2_corner_Y] = apply_homography(H, I2_corner(:,1), I2_corner(:,2));

% Calculate bbox corner for I2 in I1
I2_corner_ul = [min(I2_corner_X), min(I2_corner_Y)];
I2_corner_lr = [max(I2_corner_X), max(I2_corner_Y)];
I2_bbox_M = bbox(I2_corner_ul, I2_corner_lr);
I2_bbox_M = round(I2_bbox_M);
I2_bbox_C = bsxfun(@plus, I2_bbox_M, offset);

% Calculate new offset and update offset
offset_ul = -[min(I2_bbox_C(1,1), 0), min(I2_bbox_C(1,2), 0)];
offset_lr =  [max(I2_bbox_C(3,1), nc1), max(I2_bbox_C(3,2), nr1)];
offset = offset + offset_ul;
Isize = offset_lr + offset_ul;
nr = Isize(2);
nc = Isize(1);

% Initialize a new I that includes I1 and I2
padsize_pre = [offset_ul(2), offset_ul(1)];
padsize_post = [offset_lr(2) - nr1, offset_lr(1) - nc1];
I = padarray(I1, padsize_pre, 'pre');
I = padarray(I, padsize_post, 'post');
Imask = padarray(I1mask, padsize_pre, 'pre');
Imask = padarray(Imask, padsize_post, 'post');

% Create I2mask
[X, Y] = meshgrid(1:nc, 1:nr);
I2mask = inpolygon(X, Y, I2_corner_X+offset(1), I2_corner_Y+offset(2));

% Interpolate to get I2 in I1
X = X(:) - offset(1);
Y = Y(:) - offset(2);
[X_s, Y_s] = apply_homography(inv(H), X, Y);
I2_interp = zeros(size(I), 'uint8');
for c = 1:3
    channel = interp2(double(I2(:,:,c)), X_s, Y_s, 'linear', 0);
    I2_interp(:,:,c) = reshape(channel, nr, nc);
end

% Laplacian pyramid
% Create two masks
Bmask = Imask & I2mask;
bound = bwboundaries(Bmask);
Xbound = bound{1}(:,2);
Ybound = bound{1}(:,1);
bound_ul = [min(Xbound), min(Ybound)];
bound_lr = [max(Xbound), max(Ybound)];
% bound_corner = [bound_ul; [bound_ul(1) bound_lr(2)]; bound_lr];
% [X, Y] = meshgrid(1:nc, 1:nr);
% Bmask1 = inpolygon(X, Y, bound_corner(:,1), bound_corner(:,2)) & Bmask;
v = round((bound_ul(1)+bound_lr(1)+offset_ul(1))/2);
Bmask1 = zeros(size(Imask));
if offset(1) > 0
    Bmask1(:, v+1:end) = 1;
else
    Bmask1(:, 1:v) = 1;
end
blur = fspecial('gauss', 40, 20);
Bmask2 = 1 - Bmask1;
Bmask1b = imfilter(+Bmask1, blur, 'replicate');
% Bmask2b = 1 - Bmask1b;
Bmask2b = imfilter(+Bmask2, blur, 'replicate');
Bmask1b = repmat(Bmask1b, [1 1 3]);
Bmask2b = repmat(Bmask2b, [1 1 3]);
% Create laplacian
level = 3;
im1_lap = pyr_gen(I, level, 'lap');
im2_lap = pyr_gen(I2_interp, level, 'lap');
im_lap = cell(1,level);

for l = 1:level
    [nrl, ncl, ~] = size(im1_lap{l});
    Bmask1l = imresize(Bmask1b, [nrl, ncl]);
    Bmask2l = imresize(Bmask2b, [nrl, ncl]);
    im_lap{l} = double(im1_lap{l}).*Bmask1l + double(im2_lap{l}).*Bmask2l;
end
Iblend = pyr_recon(im_lap);

% For each pixel in I
for i = 1:nr
    for j = 1:nc
        if ~Imask(i,j) && I2mask(i,j)
            I(i,j,:) = I2_interp(i,j,:);
        elseif Imask(i,j) && I2mask(i,j)
            I(i,j,:) = Iblend(i,j,:);
%             I(i,j,:) = double(I(i,j,:)).*Bmask1b(i,j,:) + ...
%                 double(I2_interp(i,j,:)).*Bmask2b(i,j,:);
        end
    end
end
% Merge two mask
Imask = Imask | I2mask;
I = uint8(I);
%% Debug code
if 0
% figure()
% subplot(1,2,1)
% hold on
% plot(I1_corner(:,1), I1_corner(:,2), 'bo', 'MarkerFaceColor', 'b')
% plot(I2_corner_X, I2_corner_Y, 'ro', 'MarkerFaceColor', 'r')
% plot(I2_bbox_M(:,1), I2_bbox_M(:,2), 'go', 'MarkerFaceColor', 'g')
% axis ij
% axis equal
% hold off
% title('origin coordinates')
% subplot(1,2,2)
% hold on
% plot(I1_corner(:,1)+offset(1), I1_corner(:,2)+offset(2), 'bo', 'MarkerFaceColor', 'b')
% plot(I2_corner_X+offset(1), I2_corner_Y+offset(2), 'ro', 'MarkerFaceColor', 'r')
% plot(I2_bbox_M(:,1)+offset(1), I2_bbox_M(:,2)+offset(2), 'go', 'MarkerFaceColor', 'g')
% hold off
% axis ij
% axis equal
% title('canvas coordinates')
% figure()
% imshow(I1)
% 
% figure()
% imshow(I2_interp)
% 
% figure()
% colormap(gray)
% imagesc(Imask)
% axis image
% 
% figure()
% colormap(gray)
% imagesc(I2mask)
% axis image

% figure()
% imshow(Bmask)
% hold on
% plot(bound_ul(1), bound_ul(2), 'go', 'MarkerFaceColor', 'g')
% plot(bound_lr(1), bound_lr(2), 'ro', 'MarkerFaceColor', 'r')
% title('Bmask')
% axis image
% 
figure()
imshow(Bmask1b)
title('Bmask1')
axis image
% 
figure()
imshow(Bmask2b)
title('Bmask2')
axis image
% 
figure()
imshow(uint8(Iblend))
title('Iblend')
axis image
hold off

% figure()
% imshow(I)
end
end