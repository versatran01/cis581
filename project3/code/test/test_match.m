close all;
clear all;
clc;

Iall = load_images();
max_pts = 100;
for i = 1:2
    Itest{i} = Iall{i};
    im{i} = rgb2gray(Itest{i});
    cimg{i} = cornermetric(im{i}, 'Harris');
    cimg_adjusted{i} = imadjust(cimg{i});
    corners{i} = detectHarrisFeatures(im{i});
    [y{i}, x{i}, rmax{i}] = anms(cimg{i}, max_pts);
    p{i} = feat_desc(im{i}, y{i}, x{i});
end

p1 = p{1};
p2 = p{2};

thresh = 0.6;
num_corner = size(p1, 2);
m = zeros(num_corner, 1);
for i = 1:num_corner
    ssd = sum(bsxfun(@minus, p1(:,i), p2).^2, 1);
    [ssd, idx] = sort(ssd, 'ascend');
    m(i) = (ssd(1)/ssd(2) < thresh) * idx(1);
end
m(m == 0) = -1;

%display test images
figure()
for i = 1:2
    subplot(1,2,i)
    imshow(Itest{i});
    hold on
    plot(corners{i})
    hold off
    axis image;
    title(sprintf('test image raw %d', i));
    set(gca, 'Visible', 'On')
end

%corner detection
figure()
for i = 1:2
    subplot(1,2,i)
    imshow(cimg_adjusted{i});
    hold on
    plot(x{i}, y{i}, 'g+')
    hold off
    axis image;
    title(sprintf('corner metric %d', i));
    set(gca, 'Visible', 'On')
end

%adaptive non-maximal suppression
figure()
for i = 1:2
    subplot(1,2,i)
    imshow(im{i});
    axis image;
    hold on
    plot(x{i}, y{i}, 'g+')
    hold off
    title(sprintf('corner after anms %d', i));
    set(gca, 'Visible', 'On')
end

figure()
colormap(gray)
[im_height, im_width] = size(im{1});
imagesc(0, 0, im{1})
axis image
hold on
imagesc(im_width, 0, im{2})
plot(x{1}, y{1}, 'g+')
plot(x{2}+im_width, y{2}, 'g+')
plot(x{1}(m > 0), y{1}(m > 0), 'ro')
plot(x{2}(m(m > 0))+im_width, y{2}(m(m > 0)), 'ro')
xline = [x{1}(m > 0) x{2}(m(m > 0)) + im_width]';
yline = [y{1}(m > 0) y{2}(m(m > 0))]';
line(xline, yline, 'Color', 'y')
hold off
axis([0 im_width*2 0 im_height])
