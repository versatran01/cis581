function [y, x, rmax] = anms(cimg, max_pts)
% adaptive non-maximal suppression
% [y, x, rmax] = ANMS(cimg, max_pts) 

% Select corners
thresh = 0.01*max(cimg(:));
cimg(cimg < thresh) = 0;

% Reduce corner candidates by extracting regional max
bw = imregionalmax(cimg);
cimg_rm = bw.*cimg;

[r, c, v] = find(cimg_rm);
% Ascending order according to corner strength
[v, idx] = sort(v, 'ascend');
r = r(idx);
c = c(idx);

% Calculate distance squared
d2 = bsxfun(@minus, r, r').^2 + bsxfun(@minus, c, c').^2;

% Get lower triangle of d2 and set the rest to Inf
d2_tril = tril(d2, -1);
d2_tril(d2_tril == 0) = Inf;

% Get the maximum radius for each candidate corner
max_radii = min(d2_tril);
[max_radii, idx] = sort(max_radii, 'descend');

% Extract desired number of corner points
max_pts = min(max_pts, length(v));
y = r(idx(1:max_pts));
x = c(idx(1:max_pts));
rmax = sqrt(max_radii(max_pts));

end
