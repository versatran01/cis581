function corner = bbox(upper_left, lower_right)
% upper_left - upper left corner of the image [x1, y1]
% lower_right - lower right corner of the image [x2, y2]
x1 = upper_left(1);
y1 = upper_left(2);
x2 = lower_right(1);
y2 = lower_right(2);
bbox_x = [x1; x1; x2; x2];
bbox_y = [y1; y2; y2; y1];
corner = [bbox_x, bbox_y];
end