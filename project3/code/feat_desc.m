function p = feat_desc(im, y, x)
% Extract feature descriptor
% p = FEAT_DESC(im, y, x)
win_size = 40;
half_win_size = ceil(win_size/2);
y1 = y + half_win_size;
x1 = x + half_win_size;

p = zeros(64, size(y, 1));

% Create a 2d gaussian the same size as the sub image
g = fspecial('Gaussian', win_size, sqrt(2));
im_filtered = imfilter(im, g, 'symmetric', 'full');
for i = 1:size(y, 1)
    % Extract a 40x40 window around the corner
    im_sub = im_filtered((y1(i) - half_win_size):(y1(i) + half_win_size - 1), ...
                         (x1(i) - half_win_size):(x1(i) + half_win_size - 1));
    % Downsize to 8x8
    im_patch = im_sub(1:5:win_size, 1:5:win_size);
    p(:,i) = reshape(im_patch, 64, 1);
end

% Normalize to zero mean and one std
p = bsxfun(@minus, p, mean(p, 1));
p = bsxfun(@rdivide, p, std(p, 1, 1));
end
