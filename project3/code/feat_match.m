function m = feat_match(p1, p2)
% Descriptor matching
% m = FEAT_MATCH(p1, p2)
thresh = 0.6;
num_corner = size(p1, 2);
m = zeros(num_corner, 1);

for i = 1:num_corner
    ssd = sum(bsxfun(@minus, p1(:,i), p2).^2, 1);
    [ssd, idx] = sort(ssd, 'ascend');
    m(i) = (ssd(1)/ssd(2) < thresh) * idx(1);
end

m(m == 0) = -1;
end
