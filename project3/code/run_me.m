%% Test script
% Iall = load_images();
% filename = 'test';
filename = 'boat';
% filename = 'mrsl';
close all
method = 1; % Change this to see different method
% 0 for no blending
% 1 for alpha blending
% 2 for pyramid blending
load([filename, '.mat'])
img_input = Iall;
img_mosaic = mymosaic(img_input, method);
figure()
imshow(img_mosaic)
% addpath('~/Workspace/matlab/')
% git clone https://github.com/versatran01/matlab.git
% fig2pic(gcf, [filename  '_pyramid.png'], 'png', 200)

%% Readme
% Run this script to see the result
% For this project, I implemented 2 blending method, 1 is alpha blending
% 2 is pyramid blending
% As for pyramid blending, I modified code from some pyramid code I found 
% on the internet
% There are two dataset that I used, 1 is from the internet, 2 is taken by
% my cellphone. The names are boat and mrsl.