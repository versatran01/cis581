function testI = load_images(visImage)

if nargin < 1
    visImage = 0;
end

imagePath = '../images/';
imageExtension = {'.jpg', '.jpeg', '.png', '.tiff', '.bmp'};
fileListing = dir(imagePath);
testI = {};
for i = 1:length(fileListing)
    [~, ~, fileExtension] = fileparts(fileListing(i).name);
    % check if file is image
    if any(strcmp(fileExtension, imageExtension))
        testI{end+1} = imread([imagePath fileListing(i).name]);
        if visImage
            figure()
            imshow(testI{end})
            drawnow
            title(fileListing(i).name)
        end
    end
end
fprintf('Load %d images.\n', length(testI));
%close all
end
