function morphed_im = morph(im1, im2, im1_pts, im2_pts, tri, warp_frac, dissolve_frac)
% Written by Chao Qu

[im1, im2] = handle_imsize(im1, im2);
[nr, nc, ~] = size(im1);

% Get mean position in the intermidiate image
imm_pts = (1 - warp_frac)*im1_pts + warp_frac*im2_pts;
imm_pts_x = imm_pts(:,1);
imm_pts_y = imm_pts(:,2);
im1_pts_x = im1_pts(:,1);
im1_pts_y = im1_pts(:,2);
im2_pts_x = im2_pts(:,1);
im2_pts_y = im2_pts(:,2);

% Get coordinates for all pixels 
[x, y] = meshgrid(1:nc, 1:nr);
x = x(:); y = y(:);
% Find which triangle they are located in
t = tsearchn(imm_pts, tri, [x, y]);

%% Loop over each triangle
% Initialize source and target images
%morphed_im1 = zeros(size(im1), 'uint8');
%morphed_im2 = zeros(size(im2), 'uint8');
morphed_im1(nr, nc, 3) = uint8(0);
morphed_im2(nr, nc, 3) = uint8(0);
% Loop over each triangle, fast
for i = 1:size(tri, 1)
    % Create matrix A for corresponding triangle in each image
    Am = [imm_pts_x(tri(i,:))'; imm_pts_y(tri(i,:))'; ones(1, 3)];
    As1 = [im1_pts_x(tri(i,:))'; im1_pts_y(tri(i,:))'; ones(1, 3)];
    As2 = [im2_pts_x(tri(i,:))'; im2_pts_y(tri(i,:))'; ones(1, 3)];
    % Get all the points in the current triangle
    x_in_tri = x(t == i);
    y_in_tri = y(t == i);
    X = [x_in_tri'; y_in_tri'; ones(1, sum(t == i))];
    B = inv(Am)*X;
    % Calculate corresponding pixels in source images
    Xs1 = As1*B;
    Xs2 = As2*B;
    % Convert to homogenous coordinates
    Xs1 = bsxfun(@rdivide, Xs1, Xs1(3,:));
    Xs2 = bsxfun(@rdivide, Xs2, Xs2(3,:));
    % Make sure pixels are within image boundary
    Xs1 = bsxfun(@max, bsxfun(@min, round(Xs1), [nc;nr;1]), [1;1;1]);
    Xs2 = bsxfun(@max, bsxfun(@min, round(Xs2), [nc;nr;1]), [1;1;1]);
    % Copy pixel value from original image
    for j = 1:length(X)
        morphed_im1(X(2,j), X(1,j), :) = im1(Xs1(2,j), Xs1(1,j), :);
        morphed_im2(X(2,j), X(1,j), :) = im2(Xs2(2,j), Xs2(1,j), :);
    end
end

morphed_im = morphed_im1*(1 - dissolve_frac) + morphed_im2*dissolve_frac;
end

function [im1, im2] = handle_imsize(im1, im2)
[nr1, nc1, ~] = size(im1);
[nr2, nc2, ~] = size(im2);
nr = max(nr1, nr2);
nc = max(nc1, nc2);
im1 = padarray(im1, [nr-nr1, nc-nc1], 255, 'post');
im2 = padarray(im2, [nr-nr2, nc-nc2], 255, 'post');
end
