function [im1_pts, im2_pts] = click_correspondences(im1, im2)
%CLICK_CORRESPONDENCES get corresponding control points from two images
%
% [IM1_PTS, IM2_PTS] = CLICK_CORRESPONDENCES(IM1, IM2) returns position of control
% points selected from two images by hand. It uses matlab's built-in CPSELECT 
% function.
%TODO: how to resize image?
%TODO: write my own cpselect implementation

[nr1, nc1, ~] = size(im1);
[nr2, nc2, ~] = size(im2);
nr = max(nr1, nr2);
nc = max(nc1, nc2);

% Matlab function for selecting control points
[im1_pts, im2_pts] = cpselect(im1, im2, 'Wait', true);
% Add corners to control points
corners = [1, 1; 1, nr; nc, nr; nc, 1];
borders = round([1, nr/2; nc/2, nr; nc, nr/2; nc/2, 1]);
im1_pts = [im1_pts; corners; borders];
im2_pts = [im2_pts; corners; borders];

% Display control points for validation
figure(1)
ims = {im1, im2};
ctr_pts = {im1_pts, im2_pts};
for i = 1:2
    subplot(1, 2, i)
    imshow(ims{i});
    set(gca, 'Visible', 'On')
    hold on
    plot(ctr_pts{i}(:,1), ctr_pts{i}(:,2), 'c.', 'MarkerSize', 15)
    hold off
end
save('ctr_pts.mat', 'im1_pts', 'im2_pts')
end
