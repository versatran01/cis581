function morphed_im = morph_tps_wrapper(im1, im2, im1_pts, im2_pts, warp_frac, dissolve_frac)

[nr1, nc1, ~] = size(im1);
[nr2, nc2, ~] = size(im2);
nr = max(nr1, nr2);
nc = max(nc1, nc2);
sz = [nr, nc];

imm_pts = (1 - warp_frac)*im1_pts + warp_frac*im2_pts;

% im1
[a1_x, ax_x, ay_x, w_x] = est_tps(imm_pts, im1_pts(:,1));
[a1_y, ax_y, ay_y, w_y] = est_tps(imm_pts, im1_pts(:,2));
morphed_im1 = morph_tps(im1, a1_x, ax_x, ay_x, w_x, a1_y, ax_y, ay_y, w_y, imm_pts, sz);

% im2
[a1_x, ax_x, ay_x, w_x] = est_tps(imm_pts, im2_pts(:,1));
[a1_y, ax_y, ay_y, w_y] = est_tps(imm_pts, im2_pts(:,2));
morphed_im2 = morph_tps(im2, a1_x, ax_x, ay_x, w_x, a1_y, ax_y, ay_y, w_y, imm_pts, sz);

morphed_im = morphed_im1*(1 - dissolve_frac) + morphed_im2*dissolve_frac;

end