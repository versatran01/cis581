do_trig = 1;
image_path = '../images/';
im1 = imread([image_path, '01.jpg']);
im2 = imread([image_path, 'joker2.jpg']);
% [im1_pts, im2_pts] = click_correspondences(im1, im2);
load ctr_pts.mat
imm_pts = 0.5*(im1_pts + im2_pts);
tri = delaunay(imm_pts(:,1), imm_pts(:,2));
%%
h = figure(2);

if do_trig
    h_avi = avifile('Project2_my_face_trig.avi', 'fps', 10);
else
    h_avi = avifile('Project2_my_face_tps.avi', 'fps', 10);
end

for w = 0:1/60:1
    tic
    if do_trig == 0
        img_morphed = morph_tps_wrapper(im1, im2, im1_pts, im2_pts, w, w);
    else
        img_morphed = morph(im1, im2, im1_pts, im2_pts, tri, w, w); 
    end
    toc
    if w == 0
        himg = imagesc(img_morphed);
        axis image; axis off; drawnow;
    else
        set(himg, 'CData', img_morphed)
        drawnow;
    end
    h_avi = addframe(h_avi, getframe(gcf));
end
h_avi = close(h_avi);
clear h_avi;
