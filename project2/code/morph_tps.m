function morphed_im = morph_tps(im_source, a1_x, ax_x, ay_x, w_x, a1_y, ax_y, ay_y, w_y, ctr_pts, sz)

nr_m = sz(1);
nc_m = sz(2);
[nr, nc, ~] = size(im_source);
[x_m, y_m] = meshgrid(1:nc, 1:nr);
x_m = x_m(:); y_m = y_m(:);
x_c = ctr_pts(:,1);
y_c = ctr_pts(:,2);

D = bsxfun(@minus, x_c', x_m).^2 + bsxfun(@minus, y_c', y_m).^2;
U = D .* log(D);
U(isnan(U)) = 0;
% tps
x = a1_x + ax_x*x_m + ay_x*y_m + U*w_x;
y = a1_y + ax_y*x_m + ay_y*y_m + U*w_y;
% Check pixel boundary
x = min(max(round(x), 1), nc);
y = min(max(round(y), 1), nr);

%morphed_im = zeros(nr_m, nc_m, 3, 'uint8');
morphed_im(nr_m, nc_m, 3) = uint8(0); % faster matrix initialization
for i = 1:nc*nr
    morphed_im(y_m(i), x_m(i), :) = im_source(y(i), x(i), :);
end

end
