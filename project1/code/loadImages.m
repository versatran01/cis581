imagePath = '../images/';
imageExtension = {'.jpg', '.jpeg', '.png', '.tiff', '.bmp'};
fileListing = dir(imagePath);
allTestI = {};
ICount = 0;
visImage = 1;
for i = 1:length(fileListing)
    [~, ~, fileExtension] = fileparts(fileListing(i).name);
    % check if file is image
    if any(strcmp(fileExtension, imageExtension))
        ICount = ICount + 1;
        allTestI{ICount} = imread([imagePath fileListing(i).name]);
        if visImage
            figure();
            imshow(allTestI{ICount})
            drawnow
            title(fileListing(i).name)
        end
    end
end
fprintf('Load %d images.', ICount);
close all