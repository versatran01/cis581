I = allTestI{1};

figure(1);
hI = imshow(I);
set(gca, 'Visible', 'on')
[IRow, ICol, IType] = size(I);

hTitle = title('Click two points on the plot to crop the image');
xlabel(num2str(ICol))
ylabel(num2str(IRow))

% Prompt the user to crop out a sub-image
isClickValid = 0;
while ~isClickValid
    clickRow = zeros(1,2);
    clickCol = zeros(1,2);
    for i = 1:2
        [cX, cY, cB] = ginput(1);
        if cB == 3
            disp('Stopped by user.')
            % TODO: Add return in function
        end
        
        % Convert click position to image index
        % x = 0.6 or 1.1 -> col = 1
        % y = 0.6 or 1.1 -> row = 1
        clickRow(i) = round(cY);
        clickCol(i) = round(cX);
        
        % Visualize each click and save to a handle for undo
        set(hTitle, 'String', sprintf('Click %d at (%d, %d)', i, clickCol(i), clickRow(i)))
        hold on
        hClick(i) = plot(cX, cY, 'c+', 'LineWidth', 2);
        drawnow
        hold off
    end
    
    % Simplify variables here
    % r1: row index of the lower left corner of sub-image
    % c2: column index of the upper right corner of sub-image
    r1 = min(clickRow);
    r2 = max(clickRow);
    c1 = min(clickCol);
    c2 = max(clickCol);
    
    % Verify that the area formed by two clicks are valid
    isClickValid = ~((r1 > IRow) || (r2 < 1) || (c1 > ICol) || (c2 < 1));
        
    if isClickValid
        % Confine sub-image index
        r1 = max(r1, 1);
        r2 = min(r2, IRow);
        c1 = max(c1, 1);
        c2 = min(c2, ICol);
        
        % Extract Sub-image
        if IType == 1
            J = I(r1:r2, c1:c2); % Gray-scale
        elseif IType == 3
            J = I(r1:r2, c1:c2, :); % RGB
        end
        
        % Dim background image
        set(hI, 'AlphaData', 0.5)
        hold on
        image(c1, r1, J)
        hold off
        
        % Highlight cropped area
        XCrop = [c1 - 0.5, c2 + 0.5, c2 + 0.5, c1 - 0.5, c1 - 0.5];
        YCrop = [r1 - 0.5, r1 - 0.5, r2 + 0.5, r2 + 0.5, r1 - 0.5];
        hold on
        plot(XCrop, YCrop, 'k')
        hold off
        set(hTitle, 'String', ...
            sprintf('Image cropped between (%d, %d) and (%d, %d)', ...
            r1, c1, r2, c2))
    else
        delete(hClick)
        set(hTitle, 'String', 'Invalid click, please click again')
    end
end

% Display the sub-image
figure(2)
imshow(J)
set(gca, 'Visible', 'On')
title('Cropped image')