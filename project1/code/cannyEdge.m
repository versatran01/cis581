function E = cannyEdge(I)
% CANNYEDGE detects the edge of an image.
%   E = CANNYEDGE(I) computes local edge normal orientation and seeks local
%   maximum in the edge normal orientation. It continues searching in the
%   edge orientation of detected edge point.

% Written by Qu Chao, Qiong Wang
% 09/12/2013 at University of Pennsylvania

% Load image and set parameters
% Io = I;
I                   = parseImage(I);
sigma               = sqrt(2);         % Size of the standard deviation
threshRatio         = 0.4;
percentPixelNotEdge = 0.76;

% Smoothing filter and gradients
[Ix, Iy]        = myGradient(I, sigma);
Im              = hypot(Ix, Iy);
maxIm           = max(Im(:));
Im              = Im/maxIm;
Imax            = findLocalMaxima(Ix, Iy, Im);
thresh          = selectThreshold(Im, threshRatio, percentPixelNotEdge);
E               = dynamicLink(Im, Imax, Ix, Iy, thresh);
Ematlab         = edge(I, 'canny');

I2plot = {Ematlab, E};
Ititle = {'Matlab', 'cannyEdge'};
myPlot(I2plot, Ititle);
end

function Ilink = dynamicLink(Im, Imax, Ix, Iy, thresh)
phi = cart2pol(Iy, -Ix); % Direction along edge
phi2 = cart2pol(-Iy, Ix); % Direction along edge
lowThresh = thresh(1);
highThresh = thresh(2);
Ihigh = (Im.*Imax) > highThresh; % Strong pixels in local maxima
strongInd = find(Ihigh);
edgeOrientInd = findEdgeInc(phi);
edgeOrientInd2 = findEdgeInc(phi2);
isPixelVisited = zeros(size(Im));
Ilink = Ihigh;

for i = 1:length(strongInd)
    sind = strongInd(i);
    if isPixelVisited(sind)
        continue
    end
    isPixelVisited(sind) = 1;
    
    cind = sind;
    isLinkValid = 1;
    
    while isLinkValid
        nind = edgeOrientInd(cind);
        if isPixelVisited(nind) || ~Imax(nind)
            break
        end
        isPixelVisited(nind) = 1;
        % Stop if a pixel on this edge is below lower thresh
        isLinkValid = Im(nind) > lowThresh;
        if ~isLinkValid
            break
        end
        Ilink(nind) = 1;
        cind = nind;
    end
    
    cind = sind;
    isLinkValid = 1;
    
    while isLinkValid
        nind = edgeOrientInd2(cind);
        if isPixelVisited(nind) || ~Imax(nind)
            break
        end
        isPixelVisited(nind) = 1;
        isLinkValid = Im(nind) > lowThresh;
        if ~isLinkValid
            break
        end
        Ilink(nind) = 1;
        cind = nind;
    end
    
end
end

function [J, nr, nc] = parseImage(I)
% PARSEIMAGE parse the input image
if ischar(I)
    I = imread(I);
end
[nr, nc, Itype] = size(I);
if nr == 1 && nc == 1 && Itype == 1
    disp('There is no detection result');
    error('Incorrect image class');
elseif ~isa(class(I), 'single')
    J = im2single(I); % double
else
    J = I;
end

if Itype == 3
    J = rgb2gray(J);
end
end

function [Ix, Iy] = myGradient(I, sigma, param)
% MYGRADIENT calculate image gradient
if nargin < 3
    param = 'replicate';
end
% Determine filter size
Gsize = 8*ceil(sigma);
G = fspecial('gaussian', Gsize, sigma);

[dGx, dGy] = gradient(G);
Ix = imfilter(I, dGx, 'conv', param);
Iy = imfilter(I, dGy, 'conv', param);
end

function Imax = findLocalMaxima(Ix, Iy, Im)
% FINDLOCALMAXIMA finds the local maximas of gradient
if nargin < 3
    Im = hypot(Ix, Iy);
end

[nr, nc] = size(Im);
[X, Y] = meshgrid(1:nc, 1:nr);
dir1 = cart2pol(Ix, Iy);
dir2 = cart2pol(-Ix, -Iy);

% Interpolate to get pixel values on incremental direction
dir1X = min(nc, max(1, X + cos(dir1)));
dir1Y = min(nr, max(1, Y + sin(dir1)));
dir1Im = interp2(X, Y, Im, dir1X, dir1Y);
dir2X = min(nc, max(1, X + cos(dir2)));
dir2Y = min(nr, max(1, Y + sin(dir2)));
dir2Im = interp2(X, Y, Im, dir2X, dir2Y);

% Find local maximas when the current pixel bigger than both incrementals
Imax = ((Im > dir1Im) & (Im > dir2Im));
end

function neighborIndices = findEdgeInc(phi, fourNeighbors)
% ROUNDPIXEL rounds the interpolated pixel to the actual one
%
%        1 ------- 2 ------- 3
%        |         |         |
%        |         |         |
%        4 ---- current ---- 5
%        |         |         |
%        |         |         |
%        6 ------- 7 ------- 8
% 
% Here we can choose the 2, 4, 5, 7 to round the interpolated pixels as the
% other method

if nargin <4
    fourNeighbors = false;
end
[nr, nc] = size(phi);
[X, Y]   = meshgrid(1:nc, 1:nr);

if fourNeighbors
    % Index 5
    index5    = (phi > 0) & (phi < pi/4) | (phi > - pi/4) & (phi < 0);
    X(index5) = X(index5) + 1;
    
    % Index 2
    index2    = (phi > pi/4) & (phi < 3*pi/4);
    Y(index2) = Y(index2) + 1;
    
    % Index 4
    index4    = (phi > -pi) & (phi < -3*pi/4) | (phi < pi) & (phi > 3*pi/4);
    X(index4) = X(index4) - 1;
    
    % Index 7
    index7    = (phi < -pi/4) & (phi > -3*pi/4);
    Y(index7) = Y(index7) - 1;
else
    % Index 5
    index5    = (phi > 0) & (phi < pi/8) | (phi > - pi/8) & (phi < 0);
    X(index5) = X(index5) + 1;
    
    % Index 3
    index3    = (phi > pi/8) & (phi < 3*pi/8);
    X(index3) = X(index3) + 1;
    Y(index3) = Y(index3) + 1;
    
    % Index 2
    index2 = (phi > 3*pi/8) & (phi < 5*pi/8);
    Y(index2) = Y(index2) + 1;
    
    % Index 1
    index1    = (phi > 5*pi/8) & (phi < 7*pi/8);
    X(index1) = X(index1) - 1;
    Y(index1) = Y(index1) + 1;
    
    % Index 4
    index4    = (phi > -pi) & (phi < -7*pi/8) | (phi < pi) & (phi > 7*pi/8);
    X(index4) = X(index4) - 1;
    
    % Index 6
    index6    = (phi > -7*pi/8) & (phi < -5*pi/8);
    X(index6) = X(index6) - 1;
    Y(index6) = Y(index6) - 1;
    
    % Index 7
    index7    = (phi < -5*pi/8) & (phi > -3*pi/8);
    Y(index7) = Y(index7) - 1;
    
    % Index 8
    index8    = (phi > -3*pi/8) & (phi < -pi/8);
    X(index8) = X(index8) + 1;
    Y(index8) = Y(index8) - 1;
end
actualX = min(nc,max(1,X));
actualY = min(nr,max(1,Y));
neighborIndices = sub2ind([nr, nc], actualY(:), actualX(:));
end

function Ilink = linkLocalMaxima(Imax, Ix, Iy, thresh)
% ILINK links local maximas of gradient
tl        = thresh(1);
th        = thresh(2);
[nr, nc]  = size(Imax);
[X, Y]    = meshgrid(1:nc, 1:nr);

% Incremental in x and y direction along edge direction
thetaTan       = cart2pol(Iy, -Ix);
thetaTanOpp    = cart2pol(-Iy, Ix);
% cThetaTan      = cos(thetaTan);
% sThetaTan      = sin(thetaTan);
% cThetaTanOpp   = cos(thetaTanOpp);
% sThetaTanOpp   = sin(thetaTanOpp);

% Binary image through high and low threshold
Ihigh          = Imax > th;   
Ilow           = Imax > tl;   

% Rounded incrementals and their 1-D indices
% cmpTanX        = min(nc,max(1,roundPixel(X+cThetaTan)));
% cmpTanY        = min(nr,max(1,roundPixel(Y+sThetaTan)));
[cmpTanX, cmpTanY]       = roundPixelNew(X, Y, thetaTan, 1);
cmpTan                   = sub2ind([nr, nc], cmpTanY(:), cmpTanX(:));
% cmpTanXOpp     = min(nc,max(1,roundPixel(X+cThetaTanOpp)));
% cmpTanYOpp     = min(nr,max(1,roundPixel(Y+sThetaTanOpp)));
[cmpTanXOpp, cmpTanYOpp] = roundPixelNew(X, Y, thetaTanOpp, 1);
cmpTanOpp                = sub2ind([nr, nc], cmpTanYOpp(:), cmpTanXOpp(:));

% Find the edge direction incremental 1-D indices 
linkIdx        = cmpTan(Ihigh);
linkOppIdx     = cmpTanOpp(Ihigh);

% Determine the edge direction incremental pixels whether in Ilow.
link           = linkIdx(Ilow(linkIdx) == 1);
linkOpp        = linkOppIdx(Ilow(linkOppIdx) ==1);

% Initial binary edge with Ihigh and set the incremental pixel in Ilow to 1
Ilink          = Ihigh;
Ilink(link)    = 1;
Ilink(linkOpp) = 1;
end

function [actualX, actualY] = roundPixelNew(X, Y, theta, fourNeighbors)
% ROUNDPIXEL rounds the interpolated pixel to the actual one
%
%        1 ------- 2 ------- 3
%        |         |         |
%        |         |         |
%        4 ---- current ---- 5
%        |         |         |
%        |         |         |
%        6 ------- 7 ------- 8
% 
% Here we can choose the 2, 4, 5, 7 to round the interpolated pixels as the
% other method

if nargin <4
    fourNeighbors = false;
end
[nr, nc] = size(theta);

if fourNeighbors
    % Index 5
    index5    = (theta > 0) & (theta < pi/4) | (theta > - pi/4) & (theta < 0);
    X(index5) = X(index5) + 1;
    
    % Index 2
    index2    = (theta > pi/4) & (theta < 3*pi/4);
    Y(index2) = Y(index2) + 1;
    
    % Index 4
    index4    = (theta > -pi) & (theta < -3*pi/4) | (theta < pi) & (theta > 3*pi/4);
    X(index4) = X(index4) - 1;
    
    % Index 7
    index7    = (theta < -pi/4) & (theta > -3*pi/4);
    Y(index7) = Y(index7) - 1;
else
    % Index 5
    index5    = (theta > 0) & (theta < pi/8) | (theta > - pi/8) & (theta < 0);
    X(index5) = X(index5) + 1;
    
    % Index 3
    index3    = (theta > pi/8) & (theta < 3*pi/8);
    X(index3) = X(index3) + 1;
    Y(index3) = Y(index3) + 1;
    
    % Index 2
    index2 = (theta > 3*pi/8) & (theta < 5*pi/8);
    Y(index2) = Y(index2) + 1;
    
    % Index 1
    index1    = (theta > 5*pi/8) & (theta < 7*pi/8);
    X(index1) = X(index1) - 1;
    Y(index1) = Y(index1) + 1;
    
    % Index 4
    index4    = (theta > -pi) & (theta < -7*pi/8) | (theta < pi) & (theta > 7*pi/8);
    X(index4) = X(index4) - 1;
    
    % Index 6
    index6    = (theta > -7*pi/8) & (theta < -5*pi/8);
    X(index6) = X(index6) - 1;
    Y(index6) = Y(index6) - 1;
    
    % Index 7
    index7    = (theta < -5*pi/8) & (theta > -3*pi/8);
    Y(index7) = Y(index7) - 1;
    
    % Index 8
    index8    = (theta > -3*pi/8) & (theta < -pi/8);
    X(index8) = X(index8) + 1;
    Y(index8) = Y(index8) - 1;
end
actualX = min(nc,max(1,X));
actualY = min(nr,max(1,Y));   
end

function thresh = selectThreshold(Im, threshRatio, percentPixelNotEdge)
% SETTHRESHOLD sets the threshold to link local maximas
if nargin < 3
    percentPixelNotEdge = 0.7;   
end
if nargin < 2
    threshRatio = 0.4;
end

sortedIm = sort(Im(:));
highThresh = sortedIm(round(length(sortedIm)*percentPixelNotEdge));
lowThresh = threshRatio * highThresh;
thresh = [lowThresh, highThresh];
end

function myPlot(I, Ititle)
% MYPLOT plots all the verbose steps
plotLength = length(I);
numPlot = ceil(plotLength/2);
[nr, nc] = size(I{2});
for i = 1:numPlot
    figure();
    colormap(gray)
    set(gcf, 'Unit', 'Pixel', 'OuterPosition', [100 100 2*nc+50 nr+50])
    for j = 1:2
        hax(2*(i-1) + j) = subplot(1,2,j);
        imagesc(I{2*(i-1)+j});
        axis equal;
        axis([0,nc,0,nr]+0.5)
        xlabel(num2str(nc));ylabel(num2str(nr));
        title(Ititle{2*(i-1)+j})
    end
linkaxes(hax);
end
end
