function testI = load_images()
imagePath = '../images/';
imageExtension = {'.jpg', '.jpeg', '.png', '.tiff', '.bmp'};
fileListing = dir(imagePath);
testI = {};
ICount = 0;
visImage = 1;
for i = 1:length(fileListing)
    [~, ~, fileExtension] = fileparts(fileListing(i).name);
    % check if file is image
    if any(strcmp(fileExtension, imageExtension))
        ICount = ICount + 1;
        testI{ICount} = imread([imagePath fileListing(i).name]);
        if visImage
            figure();
            imshow(testI{ICount})
            drawnow
            title(fileListing(i).name)
        end
    end
end
fprintf('Load %d images.', ICount);
close all
end
