[~, Gx, Gy, Dx, Dy] = proj1_init();

S{1} = [0 0 0 0 0 1 1 1 1 1];
S{2} = [0 0 0 0 0 1 1 0 0 0 0 0];
S{3} = [0 0 0 0 0 0.5 0.5 0.5 1 1 1 1 1];

row = 4; col = 3;

figure(1)
colormap(gray)
for i = 1:col
    % Display the original signal
    subplot(row, col, i)
    imagesc(S{i})
    title(sprintf('Signal %d, original', i))
    
    % Compute where the edge is located
    subplot(row, col, col + i)
    E{i} = abs(myConv2(S{i}, Dx));
    imagesc(E{i})
    ind = find(abs(E{i} - max(E{i})) < sqrt(eps));
    for j = 1:length(ind)
        text(ind(j)-0.2, 1+0.15, num2str(ind(j)))
    end
    title(sprintf('Signal %d, edge', i))
    
    % Smooth the signal with Gaussian Kernel G
    subplot(row, col, col*2 + i)
    if i == 3
        K{i} = conv(Gx, Gx);
    else
        K{i} = Gx;
    end
    G{i} = myConv2(S{i}, K{i});
    imagesc(G{i})
    title(sprintf('Signal %d, smoothed', i))
    
    % Recompute edge location
    subplot(row, col, col*3 + i)
    EG{i} = abs(myConv2(G{i}, Dx));
    imagesc(EG{i})
    ind = find(abs(EG{i} - max(EG{i})) < sqrt(eps));
    for j = 1:length(ind)
        text(ind(j)-0.25, 1+0.15, num2str(ind(j)))
    end
    title(sprintf('Signal %d, new edge', i))
end
