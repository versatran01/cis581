% Initialize image and kernels
[I, Gx, Gy, Dx, Dy] = proj1_init();

% Calculate image gradient and magnitude
[Im, Ix, Iy] = myGradient(I, Gx, Gy, Dx, Dy);

% Display result
figure(1)
colormap(gray)
subplot(2,2,1)
imagesc(I)
title('Original image')
subplot(2,2,2)
imagesc(Im)
title('Image gradient magnitude')
subplot(2,2,3)
imagesc(Ix)
title('Image gradient x')
subplot(2,2,4)
imagesc(Iy)
title('Image gradient y')