%% create an image 	
A = [ ones(1,10); [ones(1,9),zeros(1,1)]; [ones(1,7),zeros(1,3)];...	
[ones(1,5),zeros(1,5)]; [ones(1,3),zeros(1,7)];[ones(1,1),zeros(1,9)]];	
% imagesc(A)
% colormap(gray)
%% smoothing kernel	
smoothk = [0.25, 0.5, 0.25];	

%% image I is the center portion of the image A, remove the boundary 	
I = A(2:end-1,2:end-1);	

%% compute smoothed version of the image AA, low pass	
AA = conv2(A,smoothk,'same');AA = conv2(AA,smoothk','same');	

%% take out the center portion of the smoothed image AA, 	
J = AA(2:end-1,2:end-1);	
	
%% compare the two images	
figure(1);clf;imagesc(I);colormap(gray);axis image;	
figure(2);clf;imagesc(J);colormap(gray);axis image;	

%% define image gradient operator	
dy = [1;-1];	
dx = [1,-1];	

%% compute image gradient in x and y 	
AA_y = conv2(AA,dy,'same');	
AA_x = conv2(AA,dx,'same');	

Jy = AA_y(1:end-2,2:end-1);	
Jy(1,:) = 0; % Why doing this?	

Jx = AA_x(2:end-1,1:end-2);	
Jx(:,1) = 0;	

%% display the image gradient flow	
figure(3);clf;imagesc(J);colormap(gray);axis image;	
hold on;	
quiver(Jx,Jy);	
quiver(-Jy,Jx,'r');	
quiver(Jy,-Jx,'r');	