if ~exist('testI', 'var')
    testI = load_images();
end
I = testI{4};
[~, Gx, Gy, Dx, Dy] = proj1_init();
%% Display original image
figure(1)
colormap(gray)
imagesc(I)
[IRow, ICol, IType] = size(I);
if IType == 3
    I = rgb2gray(I);
end
%% Edge detection algorithm
[Im, Ix, Iy] = myGradient(I, Gx, Gy, Dx, Dy);
[theta, rho] = cart2pol(Ix, Iy);

%% Compare my result with matlab
figure(2)
row = 3; col = 3;
% Origina image
h_ax(1) = subplot(row, col, 1);
imagesc(I)
title('Original image')

% Matlab canny edge
h_ax(2) = subplot(row, col ,2);
colormap(gray)
J1 = edge(I, 'canny');
imagesc(J1)
title('Matlab canny edge')

% Gradient x and y
h_ax(4) = subplot(row, col, 4);
imagesc(Ix)
h_ax(5) = subplot(row, col, 5);
imagesc(Iy)
h_ax(6) = subplot(row, col, 6);
imagesc(Im)

%% Link axes
linkaxes([h_ax(1) h_ax(2) h_ax(4) h_ax(5) h_ax(6)])
