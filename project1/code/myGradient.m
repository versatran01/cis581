function [Im, Ix, Iy] = myGradient(I, Gx, Gy, Dx, Dy)
%MYGRADIENT 2D  image gradient using convolution
%   [IG,IX,IY] = MYGRADIENT(I, K) calculate image gradient using
%   convolution operation
%
%   I       - original image
%   Gx, Gy  - smoothing kernel
%   Dx, Dy  - difference kernel
%   Im      - magnitude of image gradient
Gxy = conv2(Gx, Gy);
Ism = myConv2(I, Gxy);
% Ix = conv2(Ism, Dx, 'same');
% Iy = conv2(Ism, Dy, 'same');
Ix = myConv2(Ism, Dx);
Iy = myConv2(Ism, Dy);
Im = sqrt(Ix.^2 + Iy.^2);
end