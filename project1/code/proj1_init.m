function [I, Gx, Gy, Dx, Dy] = proj1_init(a)
% PROJ1_INIT initialize project 1
addpath('~/Workspace/matlab')
if nargin == 0
    a = 0.4;
end
I = [...
    1   1   1   1   0   0   1   1   0   0;
    1   1   1   1   0   0   1   1   0   0;
    1   1   1   1   0   0   1   1   0   0;
    1   1   1   1   0   0   1   1   0   0;
    0   0   0   0   0   0   0   0   0   0;
    0   0   0   0   0   0   0   0   0   0;
    0.5 0.5 0.5 1   1   1   1   1   1   1;
    0.5 0.5 0.5 0.5 1   1   1   1   1   1;
    0.5 0.5 0.5 0.5 0.5 1   1   1   1   1;
    0.5 0.5 0.5 0.5 0.5 0.5 1   1   1   1];
Gx = [1/4-a/2, 1/4, a, 1/4, 1/4-a/2];
Gy = Gx.';
Dx = [1 -1];
Dy = Dx.';
end