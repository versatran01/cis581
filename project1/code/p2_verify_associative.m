% Initialize image and kernels
[I, Gx, Gy, Dx, Dy] = proj1_init();

% Verify that convolution is associative
J1 = myConv2(myConv2(I, Gx), Gy); % conv I with Gx then with Gy
J2 = myConv2(I, conv2(Gx, Gy)); % conv Gx and Gy then with I

% Display result
error = abs(J1 - J2);
if max(error(:)) < sqrt(eps)
    disp('Convolution is associative')
else
    disp('Convolution is not associative')
end