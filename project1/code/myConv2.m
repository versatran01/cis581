function J = myConv2(I, K)
%MYCONV2 2D convolution with reflection on border
%   J = MYCONV2(I, K) convolutes the kernel K with image I using 
%   Matlab's conv2 function, but with mirror reflection when dealing
%   with borders.
%   I - original image
%   K - kernel
%   J - convoluted image

padSize = floor(size(K)/2);
padI = padarray(I, padSize, 'symmetric'); 
J = conv2(padI, K, 'same');
J = J(padSize(1)+1:end-padSize(1), padSize(2)+1:end-padSize(2));
end